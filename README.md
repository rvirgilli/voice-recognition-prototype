## Voice Recognition Prototype

#### Dependências
Arquivo 'requirements.txt'

#### Requisitos:

Os áudios, tanto de referência quanto de verificação, devem estar no formato WAV
com taxa de amostragem de 44100 Hz e bit-depth de 16 bits ou superior.

A duração mínima para o áudio de referência deve ser de 45 segundos e a duração
mínima para o áudio de verificação deve ser de 12 segundos.

#### Uso

Um exemplo de uso pode ser encontrado no arquivo 'main.py'


